# Harness

This project is a testing harness to aid in testing compilers through
generating random programs. The harness is designed for use with Emulab, and 
will form a network of machines in order to run tests as fast as possible.

The core design is a simple loop: generate a random program, feed it to a
variety of compilers, run the resulting programs, and compare their outputs. 
If the outputs differ, there is very likely a bug in one or more of the
compilers.

To drive this, the heart of the harness is a script that will test random
programs against compilers. It compares outputs as well as any inconsistent
behavior, like hangs and crashes of both the compiler and the compiled
program.

The rest of the harness extends this functionality for testing at larger
scales. With the network that the harness forms on Emulab, the first node acts
as a control node, sending instructions to the leaf nodes as well as collecting
output and hosting logging tools. Testing progress can be examined in real time
using Kibana, while the complete log is collected at the end of the campaign.


## Overview

This project is divided into two parts, profile setup and experiment

- Profile setup: Contains scripts and information to set up a profile 
  on Emulab.

- Experiment: Contains scripts and tooling that will run a fuzzing campaign in 
  parallel on multiple nodes.


## Profile Setup

The `profile_setup` directory contains useful scripts and instructions for 
setting up an emulab disk image/profile.


- `setup.sh`
    Installs necessary components and a few compilers.

- `harness_profile.py`
    The geni-lib profile specification for Emulab. Note the profile name:
    "urn:publicid:IDN+emulab.net+image..." If the disk image is saved with
    a different name, be sure to update the name in the profile script!


### Profile setup quick start

- Start up a fresh ubuntu-22 profile on Emulab.
    - Old versions of the harness use Ubuntu 18. Be aware that with old LTS versions of Ubuntu,
      a lot of tools and packages are pinned, such as Python.

- Clone this repository to your home directory.       

- Run `./harness/profile_setup/setup.sh`

- From Emulab, save the disk image with a memorable name.
    - This project uses 'xsmith-harness-ubuntu18'
    - For projects that require more modern tooling, 'UBUNTU22-64-BETA' works just fine.
    
- Make sure the profile name in harness_profile.py' matches the name of the
  disk image.
    - For example: `urn:publicid:IDN+emulab.net+image+randtest:xsmith-harness-ubuntu18`
  
- Create an experiment profile on Emulab with `harness_profile.py` as the
  source code. 
  
- To start the experiment on Emulab:
    - Select the harness profile.
    - Specify the number of nodes (at least 2).
    - Select the hardware type.
    - Finalize and start the experiment.
    

## Experiment

The `experiment` directory contains the scripts and configuration files to run
the harness.

- `ansible.cfg`
    Ansible configuration file for always-configured options.

- `config.yml`
    An example configuration file for the harness. This one is set up for a Csmith
    experiment. This file specifies options for the experiment as a whole, such as 
    the number of programs to test. It also specifies options specific to the
    harness (which lines to capture for program info), the program generator, the
    compilers, and the runtime for the generated programs.

    The configuration file is meant to be a one-stop-shop for experiment 
    configuration.
    When combined with the go button script, it is designed to make getting an 
    experiment up as painless as possible.

- `copy-snapshot.sh`/ `restore-snapshot.sh`
    Will copy/restore the ElasticSearch database to/from the local machine to 
    preserve experiment results.
    
- `go-button`
    Ties together all of the parts of the experiment and gives the user
    an easy button to press.

- `harness.py`
    Main script for running the fuzzing campaign. Can be run on its own, but
    is much more useful as part of the testing harness.

- `inventory-setup.zsh`
    Sets up the host inventory file to match however many nodes are connected.

- `run-experiment.yml`
    Ansible playbook that runs the experiment on all of the nodes and gathers
    the results back to the first node.

- `setup-experiment.yml`
    Ansible playbook that sets up all of the repositories, directories and
    testing environment for the experiment.

### Experiment quick start

- SSH into `node0` of the experiment.

- Clone this repository somewhere on that machine. An NFS mounted home 
  directory is ok.

- Navigate to the `experiment` directory.

- Edit `config.yml` and change the number of tests desired per node.
  - For Wasm fuzzing, there are a set of configs in `harness/configs/`.

- Execute `./go-button`.
  - For Wasm fuzzing, there is a executable `wasm-go-button.sh`. It will run the command:
  ```
  tmux new-session -d -s "wasm-experiment-session" \
      sudo ./go-button -i ../configs/wasmlike-reinstall.yml -c ../configs/wasm-all-config.yml
  ```
  This will allow the session to be detached for longer fuzzing campaigns. 

This will execute a small fuzzing campaign on the other nodes using default
values.

If you have a configuration that you would like to run in place of the default, 
use the command `./go-button -c <path to your config>`

The output will be located in `experiment/output`. There is a log with all
results, normal or likely bugs, as well as a log with just likely bugs.

To view the testing results in real time, point your web browser towards
`http://node0.<experiment-name>.<project-name>.emulab.net:5601`

Keep in mind that the parent process of the testing campaign will be terminated
if not detached from the SSH session using something like `nohup` or tmux.

To investigate a particular test result, run the harness on it's own with
the appropriate configuration file and the `--detailed` flag:
```
./harness -c <config_file> --detailed
```

### Architecture

Harness has four main stages: Ansible, GNU Parallel, the testing script, and
the Elastic Stack.

Ansible is used to deploy the experiment to many nodes in a cluster. It is 
responsible for setting up the repositories and environment, running the
experiment, and collecting the output logs.

GNU Parallel is called from the Ansible playbook. Its role is to run the
fuzzing campaign in parallel on the remote machines. 

The testing script is responsible for calling the random program generator,
compiling and running the random programs with different compilers, and
generating an output log.
    
The Elastic stack allows real-time collection and monitoring of fuzzing
results. 


### Configuring the experiment

The `go-button` script has a required argument for the configuration file, and an 
optional argument for an installation file:

-------------

  - `-h|--help`:             Displays a help message.
  - `-i|--install <file>`:   A YAML playbook containing installation instructions
                             to be run before the experiment. Useful for
                             keeping things up to date.
  - `-c|--config <file>`:    A configuration file containing all the
                             configuration options.



The primary way of configuring the experiments is through the options in
`config.yml`. The configuration options within affect Ansible, the harness
script, and the random program generator. The default configuration file
 provides most of the possible options as well as examples of the harness' 
 capabilities.

--------------

#### Experiment options

- `output_dir`: The name of the output directory to use for logs. When used with
                the Ansible playbook, this option will overwrite the
                harness-specific one.

- `count`:      The number of tests to run per node. Exclusive with the `duration` option.

- `duration`:   The number of seconds to run tests on worker nodes. Exclusive with the 
                `count` option. If the number of jobs give with the `jobs` option is 
                greater than the number of processors, the number of concurrent harness 
                jobs will be limited to the number of processors to prevent duration-based 
                runs from queueing up.

- `batch_size`: The number of tests to run together as a single unit. For
                responsive real-time results, set this to 1. If the count is up in the
                millions or billions, consider setting this to something like
                100. This option corresponds to the harness option of
                '--count'. The ansible playbook will overwrite the harness'
                option in order to translate the batch size to how many tests
                one invocation of the harness script should do.

- `jobs`:       The number of jobs to run concurrently on the experiment nodes.
                If this number is set to 0, the experiment will attempt to use
                all available processing power on each node. Consider setting
                this to a number a little smaller than the number of logical
                cores on each experiment machine in order to keep the machines
                responsive if a personal connection to the machine is desired.

- `server`:     Boolean for whether or not to run the generator (Xsmith specific 
                currently) in a server mode, which amortizes startup costs and allows
                very fast program generation. Only available as part of a configured 
                experiment: this option does not have an equivalent on a standalone 
                run of `harness.py`.

- `timeout`:    The number of seconds to run each command before it times out 
                and a timeout is recorded. This timeout will apply to all
                commands. There is no option to configure timeouts for
                individual commands. If functionality like that is desired, 
                setting the timeout value extremely high and writing the
                desired timeout directly in the command would be a good work
                around.

#### Harness options

Must be nested inside of a `harness` label
- `options`: Command line options for the harness. Details on these options can
             be found by running `./harness.py --help`


#### Fuzzer options

Must be nested inside of a `fuzzer` label
- `generator`: The executable of the generator to use for the experiment.
- `file-extension`: The file extension to use for generated programs.
- `options`:   Command line options to pass to the generator.

#### Startup and shutdown commands

These are commands that you wish to run at the start of and end of every testing
campaign, such as setting up a python http server that tests will query.
Startup commands must be nested inside of a `startup` label, and shutdown
commands must be nested inside of a `shutdown` label. The commands themselves
should be listed out, one command per list item. If no startup or shutdown commands
are required, simply comment out the entire block, including the 
`startup:` or `shutdown:` labels.


#### Test options

Consists of a list of test blocks, that must be nested inside of a
`tests` label. Each compiler block must contain the following:
- `name`: The name of the test run
- `commands`:  This label must contain a list of commands, each with a user
               defined name and a shell command to run in order. Note that 
               most shell builtins are not available, like file redirection.

The `tests` section evaluates the generated random programs, and reports back
if there were crashes, inconsistent hangs, inconsistent output, or if
everything went well. There are a few features to be aware of in this section.

Random program generator output can be referred to in the commands with 
`$fuzzer-output`. This is the only explicitly reserved keyword.

The stdin and stdout of other steps can be referred to as files by their 
command name, and the type of output, separated by a period. The types of 
output are `stdout` and `stderr`. For referencing a file that was created 
by an earlier command, simply use the name of the file. 
For example: `$<previous-command-name>.stdout`.

References to harness specific variables like the fuzzer output and the 
outputs of each command start with a dollar sign. If you wish to use 
environment variables in your commands, preface them with two dollar signs 
instead of one, like so: `$$HOME` instead of `$HOME`.

Subshell commands are allowed, with a few restrictions. Subshells must be 
escaped with an additional dollar sign, like environment variables: 
`$$( <subshell command> )`. Nesting subshells is not supported. Subshells 
have their working directory set to the current test subdirectory. If you 
are looking for a file that you created in an earlier command, you are in 
the same directory.

The harness will turn relative references into absolute paths behind 
the scenes. If you need the full path for some reason (like telling a 
webserver where to look) you can use a builtin command like `realpath`. For 
example, to get the absolute path of a generated test file and chop off the 
first few directories because it's being served by some webserver, there are 
a few steps needed.

```
- name: path example
    commands:
      - generate: cp $fuzzer-output test_wasm.wasm
      - program_path: realpath test_wasm.wasm
      - output: cat $$( sed s,/local/work,, $program_path.stdout )
```

Note references to harness specific variables like the fuzzer output and the 
outputs of each command start with a dollar sign. If you wish to use
environment variables in your commands, preface them with two dollar signs
instead of one, like so: `$$HOME` instead of `$HOME`

For evaluation, the harness will track crashes and timeouts, and will compare
the outputs of each of the last steps in each commands. If the outputs do
not match, the harness will report a wrong code bug.


#### Configuring the elastic stack

The provided configuration files are lightly modified default configuration
files. They should not be modified unless absolutely needed.

The likely exception to this rule is `base_logstash.conf`. This is a Logstash
pipeline configuration file, and provides the parsing instructions that
separate useful data from the human-readable log message for use in Elastic
Search and Kibana. 

To make your own modifications to the logging pipeline, consider
creating your own configuration file instead of modifying the existing configuration. 
The file extension must be `.conf` and it will
be used in addition to the existing rules in `base_logstash.conf`. See the
[Logstash documentation](https://www.elastic.co/guide/en/logstash/current/configuration.html)
for more details on how to parse and process logs. To use the added rules, the config
file must be copied to the `/etc/logstash/conf.d/` directory on the central node. 
The best place to do this is likely in your experiment-specific installation or
re-installation script.

When the harness is running, the logstash configuration files will be concatenated in
alphabetical order. In order to make sure that the log parsing is predictable, and that
uses of fields already parsed by the base configuration are available, a recommended
naming structure to a user-defined config file is `post_<...>.conf`. See [this StackOverflow
post](https://stackoverflow.com/questions/46977084/logstash-define-field-within-a-field)
for an example.

When viewing testing results through Kibana, there are a few example visualizations
provided. One is using Timelion, in which the graphs are described by a chain
of functions. The documentation for these functions is sparse at best, though. The
second visualization uses built-in graphical tools, and is probably the example
to work off of.


#### Save your data!

Be sure to move/copy the resulting log files out of the configured output directory 
after running a fuzz campaign. When you run a fuzz campaign with the 
`go-button`, it will delete all files in the configured output directory, 
including any log files from a previous run.
Copy that precious result data to another machine! There are two scripts
provided for this purpose: `copy-snapshot.sh` and `restore-snapshot.sh`. These
scripts contain a few useful commands to copy the elastic stack database out
and back in.


#### Debugging Logstash Configurations

Debugging the logstash configuration files is more involved than debugging a python program. If 
there is an error in the configuration, logstash will silently crash. No output will reach the
kibana database.

First, start by checking the logs on the experiment. Since logstash is run as a service, and not
a process, the logs are held by journalctl and will not be found in the usual place. To see the
logs, run this command: `sudo journalctl -u logstash.service`

To check the validity of a logstash configuration, you will need to run logstash in
a configuration testing mode. This can be done on your local machine or the testing machines. On an
emulab node: `sudo /usr/share/logstash/bin/logstash --config.test_and_exit -f <path_to_config_file>`

If the configuration tester reports that everything is fine, the next thing to check is that the
configuration actually does what you want it to. Start by running your experiment, and collecting
a log that contains examples of output that logstash does not treat correctly. For example, on node1
in the fuzzing experiment setup, a log can be found at `/local/work/output1/bug-log.txt`. Save this
log, and copy it into a [grok debugging website](https://grokconstructor.appspot.com/do/match).
Then copy in the grok portion of the configuration you want to debug. For example, the pattern that
should match all log lines is `(?m)\[[-!]{4}\] %{DATA:test_result_string}\n%{GREEDYDATA:test_log}`.
Since all log lines start witha variation of `[-!!-]`, set the multiline pattern to `^\[`, and set
the 'negate' option to true. There should be some kind of output telling you which pieces of data 
got put into which variables. 

If the configuration tester is reporting that the configuration is ok, and the grok debugger shows
the log fields being properly separated, but there is still something wrong in the output, like
a filter not changing the 'test-result' tag for a false positive, you will need to look at the debug
output for logstash. I recommend doing this on your own machine, with a standalone logstash process
instead of the daemon logstash process on the testing machines.

Replace the input section of the logstash configuration with the following:

```
input{
  stdin {
    codec => multiline {
      pattern => "^\["
      negate => "true"
      what => "previous"
    }
  }
}
```

Replace the output section of the logstash configuration with the following:

```
output {
  stdout { codec => rubydebug }
}
```

Run logstash with the following command:

```
logstash -r -f yourconfig.conf > debugfile.out
```

The `-r` reloads the config file into logstash if you change it. Open another terminal window and
use `tail -f debugfile.out` to view the output. Paste in the logs that cause the problem to the
original terminal. Because the logs are multiline, and must be separated with a newline, be careful
of extra or missing newlines. Because logstash is looking for the end of a multiline log entry,
you will need to input another log entry (or just `[]`) before logstash will process the first 
one. To get `printf` style debugging statements, you can use Ruby printing,
like this:

```
filter { 
  ...
  ruby {
    code => "puts event['message']"
  }
}
```
