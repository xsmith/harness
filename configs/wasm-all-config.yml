# Configuration file for a wasmer test run on the harness
# This config file is a subset of the tests that wasmer will actually run without crashing
# See https://github.com/wasmerio/wasmer/issues/2590 for details

# Since this file is used both by ansible and the harness script,
# it is expected to be in 'harness/experiment/'.

# Changes made to this configuration file will be
# propagated to the other nodes in the experiment when run


## Experiment runtime options
output_dir: output
count: 200000  # Either 'count' or 'duration'. 'count' is per node, 'duration is in seconds'

#duration: 10
batch_size: 1
jobs: 30  # 0 means run as many as possible
server: False  # Will query an xsmith server for generating programs. Can be significantly faster.
               # !!! The harness will assume that the server is up if passed this flag.


## Harness runtime options
harness:
  options:
    - "--min-program-size 10"
    - "--info-lines [2,4]"
    - "--timeout 10"  # Anything below 10 will result in Firefox not loading.
    - "--verbose" # Required for the browsers

fuzzer:
  # Will use the default path to find the executable
  # If using the server mode, the options must be absolute paths
  generator: racket
  file-extension: ".wat"
  options:
    - "/local/work/webassembly-sandbox/wasmlike/wasmlike.rkt"
    - "--max-depth 9 --function-definition-falloff 4 --with-floating-point true"

startup:
  # TODO: use the racket server mode
  # For the browsers, we need to serve the directory containing all of the outputs so the browser
  # can see them. The paths are relative to the webpage loaded, which means we want to copy both the
  # html and the load-playwright script to /local/work/
  #
  # All of the commands here are prefixed with nohup by the test harness. If an environment variable
  # is needed, use `env FOO=bar command`
  - "cp /local/work/webassembly-sandbox/playwright/wasm-webpage.html /local/work/wasm-webpage.html"
  - "cp /local/work/webassembly-sandbox/playwright/load-playwright.js /local/work/load-playwright.js"
  - "env PLAYWRIGHT_BROWSERS_PATH=/local/work/pw-browsers npx playwright install"
  - "sudo npx pm2 start http-server -- /local/work -p 8080"
  #- "python3.10 -m http.server --directory /local/work/ 8080 2>&1 > /local/work/server_log.txt &"
  - "raco make /local/work/webassembly-sandbox/wasmlike/wasmlike.rkt"
  - "cargo build 
       --manifest-path /local/work/webassembly-sandbox/wasmtime/load-wasmtime/Cargo.toml
       --release"
  - "env LLVM_SYS_110_PREFIX='/usr/lib/llvm-12/'
       cargo build
       --manifest-path /local/work/webassembly-sandbox/wasmer/load-wasmer/Cargo.toml
       --release"

tests:
  - name: "NodeJS"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - runtime: >  # multiline -> one line
          node /local/work/webassembly-sandbox/node/load-node.js test_wasm.wasm

  ## Cranelift
  - name: "Wasmer cranelift"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - runtime: >
          /local/work/webassembly-sandbox/wasmer/load-wasmer/target/release/load-wasmer test_wasm.wasm
          --compiler cranelift

  - name: "Wasmer cranelift/optimize"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - runtime: >
          /local/work/webassembly-sandbox/wasmer/load-wasmer/target/release/load-wasmer test_wasm.wasm
          --compiler cranelift
          --optimize

  ## LLVM
  - name: "Wasmer llvm"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - runtime: >
          /local/work/webassembly-sandbox/wasmer/load-wasmer/target/release/load-wasmer test_wasm.wasm
          --compiler llvm

  - name: "Wasmer llvm/optimize"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - runtime: >
          /local/work/webassembly-sandbox/wasmer/load-wasmer/target/release/load-wasmer test_wasm.wasm
          --compiler llvm
          --optimize

  - name: "Wasmtime"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - runtime: >
          /local/work/webassembly-sandbox/wasmtime/load-wasmtime/target/release/load-wasmtime test_wasm.wasm

  - name: "Wasmtime optimize"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - runtime: >
          /local/work/webassembly-sandbox/wasmtime/load-wasmtime/target/release/load-wasmtime test_wasm.wasm
          --optimize

  - name: "Firefox"
    commands:
      - cleanup: |
          tmpreaper 10m /local/work/firefox_profiles/;
          tmpreaper 10m /local/tmp/;
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - playwright: >
          env PLAYWRIGHT_BROWSERS_PATH=/local/work/pw-browsers 
            node /local/work/webassembly-sandbox/playwright/sock-puppet.js
            -b firefox
            -p http://localhost:8080/wasm-webpage.html
            # Get the full path, and chop off `/local/work/`
            -f $$( path=`realpath test_wasm.wasm`; echo $${path#/local/work/} )

  - name: "Chromium"
    commands:
      - compiler: wat2wasm $fuzzer-output -o test_wasm.wasm
      - playwright: >
          env PLAYWRIGHT_BROWSERS_PATH=/local/work/pw-browsers 
            node /local/work/webassembly-sandbox/playwright/sock-puppet.js
            -b chromium
            -p http://localhost:8080/wasm-webpage.html
            -f $$( path=`realpath test_wasm.wasm`; echo $${path#/local/work/} )

shutdown:
  - "sudo npx pm2 stop http-server"
  #- "pkill -f 'npx http-server /local/work -p 8080'"
  #- "pkill -f 'python3.10 -m http.server --directory /local/work/ 8080'"
  - echo "Done!"

