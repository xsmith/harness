Copy the `Makefile` to a useful directory with write permissions and run `make`.
It will automatically install a few Python versions into separate directories
without installing them globally.

There is some setup to be done before running the Makefile. Note that there are
a few variables that should be declared here:

  * `DIR` is the top directory under which you want to install all the Python
    versions.
  * `MKFILE` is the path to the Makefile stored in this directory.
  * `GROUP` is the name of a user group on the target system that will be given
    read/write access to all the installations.

```
$ sudo apt-get update
$ sudo apt-get install -y libbz2-dev libsqlite3-dev libgdbm-dev tk-dev libgc-dev liblzma-dev libncursesw5-dev
$ export DIR=<root directory>
$ export MKFILE=<path to Makefile>
$ export GROUP=<group name to use for work>
$ cd "$DIR"
$ sudo mkdir pythons
$ sudo chgrp "$GROUP" pythons
$ sudo chmod g+ws pythons
$ cd pythons
$ mv "$MKFILE" .
$ make
```
