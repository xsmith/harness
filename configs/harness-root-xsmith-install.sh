#!/usr/bin/env bash

# This should be run as root, which it will be if run by ansible.
# Install xsmith and xsmith-examples repos as root.
# This makes them available to the harness fuzzing processes.

git clone https://gitlab.flux.utah.edu/xsmith/xsmith/ /xsmith
cd /xsmith/xsmith
raco pkg install --auto
cd ../xsmith-examples
raco pkg install --auto
