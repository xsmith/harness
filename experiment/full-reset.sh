#!/usr/bin/env sh
ansible all -m file --become -a 'path=/local/harness.setup_run state=absent'
ansible all -m file --become -a 'path=/local/harness.wasm_install_run state=absent'
ansible all -m file --become -a 'path=/local/work state=absent force=true'
