# This script will copy the harness elastic-search repository to ./harness_backup
# It is intended for use on the root node of the experiment

mkdir -p ./harness_backup

sudo cp -r /etc/elasticsearch/snapshot/harness_backup ./harness_backup

