#!/usr/bin/env zsh

# The inventory file for this project is super simple, but could be variable-sized
# This script will generate the proper inventory file automatically

NUM_HOSTS=0

# Ping and ignore everything but the return code
while ping -c 1 node${NUM_HOSTS} > /dev/null 2>&1; do
    ((NUM_HOSTS++));
done

#ignore response from node0
((NUM_HOSTS--))


# Overwrite and start with a new file
echo "[central]" > hosts;
echo "node0" >> hosts;
echo "" >> hosts;
echo "[experiment]" >> hosts;
echo "node[1:${NUM_HOSTS}]" >> hosts;

echo "Inventory setup successful!"

