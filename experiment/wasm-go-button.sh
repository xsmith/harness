#!/usr/bin/env bash

tmux new-session -d -s "wasm-experiment-session" \
  sudo ./go-button -i ../configs/wasmlike-reinstall.yml -c ../configs/wasm-all-config.yml

tmux attach -t wasm-experiment-session
