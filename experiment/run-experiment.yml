- name: Update ownership permissions
  hosts: all
  tasks:
  - name: Change ownership of /local to current user
    become: yes
    shell:
      cmd: chown -R $(whoami):$(groups | awk '{print $1') /local


- name: Start Elastic Stack components on central node
  hosts: central
  tasks:
  - name: Start Elastic Search service
    become: yes
    service:
      name: elasticsearch
      state: started

  - name: Start Kibana service
    become: yes
    service:
      name: kibana
      state: started

  - name: Wait for Kibana server to start
    wait_for:
      host: localhost
      port: 5601
      delay: 5
      timeout: 300

  - name: Wait 30 more seconds for Kibana to finish setting up
    pause:
      seconds: 30

  - name: Import kibana dashboard
    become: yes
    shell:
      cmd: >
        curl -X POST 'http://localhost:5601/api/saved_objects/_import?overwrite=true' 
        -H 'kbn-xsrf: true' 
        --form file=@/etc/kibana/kibana-settings.ndjson

  - name: Start Logstash service
    become: yes
    service:
        name: logstash
        state: restarted

- name: Start Elastic Stack components on leaf nodes
  hosts: experiment
  tasks:
  - name: Start Filebeat service
    become: yes
    service:
      name: filebeat
      state: started

  - name: Pause to let Elastic services get set up
    pause:
      seconds: 10

- name: Run fuzzing experiment
  hosts: experiment
  vars:
    config_file: config.yml  # Will be overwritten with '--extra-vars' from the go button script
  tasks:
  - name: Copy local '{{ config_file }}' to remote nodes
    copy:
      src: "{{ config_file }}"
      dest: /local/work/harness-run-config.yml
      force: yes

  - name: Include variables from '{{ config_file }}' into the 'config' variable
    include_vars:
      file: "{{ config_file }}"
      name: config

  - name: Debug the config variable
    debug:
      var: config
      verbosity: 1

  - name: Update harness directory on experiment nodes with local changes on central node
    # {{ playbook_dir }} should be .../harness/experiment
    # -r: recursive
    become: yes
    shell:
      cmd: >
        rsync -r
        $(realpath {{ playbook_dir }}/..)
        {{ inventory_hostname }}:/local/work
    delegate_to: node0
    # delegate_to means that each experiment node will set up the command with their
    #   variables, but have the central node execute the command.

  - name: Clean up past output
    become: yes
    shell:
      chdir: /local/work
      cmd: |
        rm -f parallel_input
        rm -rf {{ config.output_dir }}-*
        rm -f bug-log-combined.txt
        rm -f full-log-combined.txt

  - name: debug options
    become: yes
    shell:
      chdir: /local/work
      executable: /bin/bash
      cmd: |
        echo "{{ config.fuzzer.options }}" > debug_options.txt

  - name: Start the Xsmith server
    become: yes
    shell:
      chdir: /local/work
      executable: /bin/bash
      cmd: |
        rm -f /local/work/fuzzer.pid
        start-stop-daemon --start \
        --startas "$(which {{ config.fuzzer.generator }})" \
        --make-pidfile --pidfile /local/work/fuzzer.pid \
        --background \
        -- {{ config.fuzzer.options | join(" ") }} --server true --server-path "/"; \
        sleep 5 
    when: (config.server is defined) and (config.server == true)
    # The sleep is necessary to let the daemon command finish running

  - name: Run startup commands
    become: yes
    shell:
      chdir: /local/work
      executable: /bin/bash
      cmd: "nohup {{ item }}" # nohup means do not stop when the user (ansible command) logs out
    loop: "{{ config.startup }}"
    when: config.startup is defined
    register: startup_commands
    until: startup_commands is not failed
    retries: 3

  - name: Run count-based fuzzing campaign
    block:
      - name: Set up parallel tasks
        become: yes
        shell:
          chdir: /local/work
          cmd: |
            rm -f parallel_input;
            for i in $(seq {{ config.count|int // config.batch_size|int }}); do
              echo {{ config.batch_size|int }} >> parallel_input;
            done;
            if [ {{ config.count|int % config.batch_size|int }} -gt 0 ]; then
              echo {{ config.count|int % config.batch_size|int }} >> parallel_input;
            fi
          executable: /bin/bash
      - name: Debug test suite command
        debug:
          msg: |
            parallel \
              --jobs {{ config.jobs|int }} \
              -I @@ \
              --slotreplace ,, \
              -a parallel_input \
              --tmpdir /local/tmp \
              --compress \
            ./harness/experiment/harness.py \
                --config /local/work/harness-run-config.yml \
                --output-dir {{ config.output_dir }}-,, \
                --count @@

      - name: Run the count-based test suite
        become: yes
        shell:
          chdir: /local/work
          executable: /bin/bash
          cmd: |
            parallel \
              --jobs {{ config.jobs|int }} \
              -I @@ \
              --slotreplace ,, \
              -a parallel_input \
              --tmpdir /local/tmp \
              --compress \
            ./harness/experiment/harness.py \
                --config /local/work/harness-run-config.yml \
                --output-dir {{ config.output_dir }}-,, \
                --count @@
    when: config.count is defined

  - name: Run duration-based fuzzing campaign
    block:
      - name: Set up parallel tasks
        become: yes
        shell:
          chdir: /local/work
          cmd: |
            rm -f parallel_input;
            MAX_JOBS=0;
            if [ {{ config.jobs|int }} -lt $(nproc) ]; then
              MAX_JOBS={{ config.jobs|int }};
            elif [ {{ config.jobs|int }} -eq 0 ]; then
              MAX_JOBS=$(nproc);
            elif [ {{ config.jobs|int }} -gt $(nproc) ]; then
              MAX_JOBS=$(nproc);
            else
              MAX_JOBS={{ config.jobs|int }};
            fi;

            for i in $(seq $MAX_JOBS); do
              echo 1 >> parallel_input;
            done;
          executable: /bin/bash

      - name: Debug parallel input generator
        debug:
          msg: |
            rm -f parallel_input;
            MAX_JOBS=0;
            if [ {{ config.jobs|int }} -lt $(nproc) ]; then
              MAX_JOBS={{ config.jobs|int }};
            elif [ {{ config.jobs|int }} -eq 0 ]; then
              MAX_JOBS=$(nproc);
            elif [ {{ config.jobs|int }} -gt $(nproc) ]; then
              MAX_JOBS=$(nproc);
            else
              MAX_JOBS={{ config.jobs|int }};
            fi;

            for i in $(seq $MAX_JOBS); do
              echo 1 >> parallel_input;
            done;

      - name: Debug test suite command
        debug:
          msg: |
            parallel \
              --jobs {{ config.jobs|int }} \
              -I @@ \
              --slotreplace ,, \
              -a parallel_input \
              --tmpdir /local/tmp \
              --compress \
            ./harness/experiment/harness.py \
                --config /local/work/harness-run-config.yml \
                --output-dir {{ config.output_dir }}-,, \
                --duration {{ config.duration|int }}


      - name: Run the duration-based test suite
        shell:
          chdir: /local/work
          executable: /bin/bash
          # no-backup prevents the system from getting filled up with backups of past runs
          cmd: |
            parallel \
              --jobs {{ config.jobs|int }} \
              -I @@ \
              --slotreplace ,, \
              -a parallel_input \
              --tmpdir /local/tmp \
              --compress \
            ./harness/experiment/harness.py \
                --config /local/work/harness-run-config.yml \
                --output-dir {{ config.output_dir }}-,, \
                --duration {{ config.duration|int }}
    when: config.duration is defined
    #register: out
  #- debug: var=out.stdout_lines

  - name: Stop the Xsmith server
    become: yes
    shell:
      chdir: /local/work
      executable: /bin/bash
      cmd: |
        start-stop-daemon --stop \
        --remove-pidfile --pidfile /local/work/fuzzer.pid; \
        sleep 5
    when: (config.server is defined) and (config.server == true)

  - name: Run shutdown commands
    become: yes
    shell:
      chdir: /local/work
      executable: /bin/bash
      cmd: "{{ item }}"
    loop: "{{ config.shutdown }}"
    when: config.shutdown is defined


  - name: Concatenate the output logs
    shell:
      chdir: /local/work
      executable: /bin/bash
      cmd: |
        cat {{ config.output_dir }}-*/full-log.txt > full-log-combined.txt
        cat {{ config.output_dir }}-*/bug-log.txt > bug-log-combined.txt
      
  - name: Copy back the combined log
    fetch:
      src: /local/work/full-log-combined.txt
      dest: "{{ config.output_dir }}/{{ inventory_hostname }}-full-log.txt" # this path is relative to the playbook
      flat: yes
  
  - name: Copy back the combined bug log
    fetch:
      src: /local/work/bug-log-combined.txt
      dest: "{{ config.output_dir }}/{{ inventory_hostname }}-bug-log.txt" # this path is relative to the playbook
      flat: yes
  
  - name: Combine output logs on the host
    local_action:
      module: shell
      cmd: |
        cat {{ config.output_dir }}/*-full-log.txt > {{ config.output_dir }}/full-log-combined.txt
        cat {{ config.output_dir }}/*-bug-log.txt > {{ config.output_dir }}/bug-log-combined.txt

- name: Save a snapshot of the elasticsearch data
  hosts: central
  tasks:

  - name: Register the snapshot directory if it isn't already
    become: yes
    shell:
      creates: /etc/elasticsearch/snapshot/harness_backup
      cmd: |
          curl -X PUT "localhost:9200/_snapshot/harness_backup" -H 'Content-Type: application/json' \
          -d '{ "type": "fs", "settings": { "location": "harness_backup" } }'

  - name: Save a snapshot in ElasticSearch
    become: yes
    shell:
      cmd: |
          curl -X DELETE "localhost:9200/_snapshot/harness_backup/snapshot"; \ 
          curl -X PUT "localhost:9200/_snapshot/harness_backup/snapshot?wait_for_completion=false"


 #How to glob items on a remote host
  # - debug:
  #     var: item.path
  #   with_items: "{{ output_files.files }}"
     
  # - name: Copy back the results
  #   fetch:
  #     src: "{{ item }}"
  #     dest: /tmp
  #   with_fileglob:
  #       - /local/work/fuzzer/scripts/test*.c

