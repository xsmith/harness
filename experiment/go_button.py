#!/usr/bin/env python3

import subprocess
import sys

from enum import Enum
from os import environ, geteuid
from pathlib import Path
from shutil import rmtree
from typing import List, Optional, Union


EXPERIMENT_DIR = Path(__file__).parent.resolve().absolute()

ANSIBLE_CFG         = EXPERIMENT_DIR / 'ansible.cfg'
INVENTORY_SCRIPT    = EXPERIMENT_DIR / 'inventory-setup.zsh'
EXPERIMENT_PLAYBOOK = EXPERIMENT_DIR / 'setup-experiment.yml'
RUN_PLAYBOOK        = EXPERIMENT_DIR / 'run-experiment.yml'

DEFAULT_CONFIG_FILE = EXPERIMENT_DIR / 'config.yml'
DEFAULT_OUTPUT_DIR  = EXPERIMENT_DIR / 'output'

PLAYBOOK_CMD        = 'ansible-playbook'
ANSIBLE_SETUP_LOG   = 'ansible-setup.log'
ANSIBLE_INSTALL_LOG = 'ansible-install.log'
ANSIBLE_RUN_LOG     = 'ansible-run.log'

# Extend the current environment with the ANSIBLE_CONFIG variable to override
# Ansible's default behavior.
ANSIBLE_ENV         = {'ANSIBLE_CONFIG': str(ANSIBLE_CFG),
                       **environ}


class ErrorCode(Enum):
    NOT_RUN_AS_ROOT           = 1
    OUTPUT_DIR_CLEANUP_FAILED = 2
    OUTPUT_DIR_NOT_CREATED    = 3
    INVENTORY_SETUP_FAILED    = 4
    SETUP_FAILED              = 5
    INSTALL_FAILED            = 6
    CAMPAIGN_FAILED           = 7


def eprint(*args, **kwargs):
    kwargs['file'] = sys.stderr
    print(*args, **kwargs)


def exit(error_code: ErrorCode):
    sys.exit(error_code.value)


def assert_user_is_root():
    if not geteuid() == 0:
        eprint("ERROR: This script must be run as root!")
        exit(ErrorCode.NOT_RUN_AS_ROOT)


def remove_and_replace_output_dir(output_dir: Path):
    if output_dir.exists():
        print("Clearing existing output directory...")
        try:
            rmtree(output_dir)
        except OSError as e:
            eprint(f"ERROR: Could not remove output directory: {output_dir}")
            eprint(e.strerror)
            exit(ErrorCode.OUTPUT_DIR_CLEANUP_FAILED)
        print("Cleared.")
    else:
        print("No existing output directory to clear.")
    try:
        output_dir.mkdir()
    except OSError as e:
        eprint(f"ERROR: Could not create output directory: {output_dir}")
        eprint(e.strerror)
        exit(ErrorCode.OUTPUT_DIR_NOT_CREATED)
    print("Clean output directory prepared.")


def run_command(command: Union[Path, str, List[Union[Path, str]]],
                step_name: str,
                error_code: ErrorCode,
                output_file: Optional[Path] = None):
    status_line = f"Beginning {step_name.lower()}"
    if output_file is not None:
        status_line += f" with output to: {output_file}"
    status_line += "..."
    print(status_line)
    # The subprocess module doesn't handle Paths correctly, so convert them.
    if isinstance(command, Path):
        command = str(command)
    elif isinstance(command, list):
        for i, part in enumerate(command):
            if isinstance(part, Path):
                command[i] = str(part)
    # Use stdout if no output file is specified.
    try:
        output_fd = sys.stdout if output_file is None else open(output_file, 'w')
        subprocess.run(command, stdout=output_fd, env=ANSIBLE_ENV)
    except subprocess.CalledProcessError as e:
        eprint(f"ERROR: {step_name.capitalize()} failed with error code: {e.returncode}.")
        if output_file is not None:
            eprint(f"       See output in {output_file} for more details.")
        exit(error_code)
    finally:
        # Only close the output file if it's not stdout!
        if output_file is not None:
            output_fd.close()
    print(f"{step_name.capitalize()} completed.")


def set_up_inventory():
    run_command(
        command=INVENTORY_SCRIPT,
        step_name="inventory setup",
        error_code=ErrorCode.INVENTORY_SETUP_FAILED)


def set_up_experiment(output_dir: Path):
    run_command(
        command=[PLAYBOOK_CMD, EXPERIMENT_PLAYBOOK],
        step_name="experiment set-up",
        error_code=ErrorCode.SETUP_FAILED,
        output_file=output_dir / ANSIBLE_SETUP_LOG)


def install_experiment(install_playbook: Path, output_dir: Path):
    run_command(
        command=[PLAYBOOK_CMD, install_playbook],
        step_name="experiment installation",
        error_code=ErrorCode.INSTALL_FAILED,
        output_file=output_dir / ANSIBLE_INSTALL_LOG)


def run_testing_campaign(config_file: Path, output_dir: Path):
    run_command(
        command=[PLAYBOOK_CMD, RUN_PLAYBOOK],
        step_name="testing campaign",
        error_code=ErrorCode.CAMPAIGN_FAILED,
        output_file=output_dir / ANSIBLE_RUN_LOG)


def execute_experiment(config_file: Path, output_dir: Path, install_playbook: Optional[Path]):
    remove_and_replace_output_dir(output_dir)
    set_up_inventory()
    set_up_experiment(output_dir)
    if install_playbook is not None:
        install_experiment(install_playbook, output_dir)
    run_testing_campaign(config_file, output_dir)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="A script for managing the execution of a harness testing campaign.")
    parser.add_argument('-c', '--config', type=Path, default=DEFAULT_CONFIG_FILE,
                        help=("configuration file containing variables for both the Ansible and harness runtimes;"
                              f" the default is: {DEFAULT_CONFIG_FILE}"))
    parser.add_argument('-i', '--install', type=Path,
                        help="Ansible playbook containing install tasks to run; none is provided by default")
    parser.add_argument('-o', '--output', type=Path, default=DEFAULT_OUTPUT_DIR,
                        help=f"directory for outputting Ansible log files during execution; the default is: {DEFAULT_OUTPUT_DIR}")
    args = parser.parse_args()

    # Only root should execute this script.
    assert_user_is_root()

    # Begin running all parts of the experiment.
    execute_experiment(config_file=args.config,
                       output_dir=args.output,
                       install_playbook=args.install)
