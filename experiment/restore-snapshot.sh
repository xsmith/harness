# This script will copy a given backup to the proper location and tell elasticsearch to restore
# from the data. It is designed to run on the root node of the experiment. 
# !!! Restoring the backup will overwrite anything currently there!
# TODO: this functionality could be expanded into it's own ansible playbook, but this works well enough

if [ $# -gt 1 ] || [ $# -eq 0 ] || [ "$1" == "-h" ]; then
    echo "Usage: restore-snapshot <path-to-snapshot-dir>"
    exit 1
fi

sudo rm -rf /etc/elasticsearch/snapshot/*
sudo cp -r "$1" /etc/elasticsearch/snapshot/harness_backup

curl -X POST "localhost:9200/_snapshot/harness_backup/snapshot/_restore"


