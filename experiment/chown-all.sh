#!/usr/bin/env sh
ansible all -m shell --become -a "chown -R $(whoami):$(groups | awk '{print $1}') /local"
