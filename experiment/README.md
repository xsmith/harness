## Experiment Setup

Most of the experiment is setup automatically. If you want to include your
own complex setup instructions , the setup-experiment.yml fil is a good starting 
point, and includes examples of using shell commands and apt installs.

A better approach for simpler setup of an experiment would be to install what you
need through ansible ad-hoc commands then save the image in emulab for future
experiments.

Help for ansible ad-hoc commands can be found [here](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html).

The basic form is: `ansible [pattern] -m [module] -a "[module options]"`. 

The pattern is which nodes to target. When run in this directory, "central" refers
to the central node, "experiment" refers to the leaf nodes, and "all" refers to 
everything.

The default module is the `command` module. The `command` module will execute the first
argument as the name of the binary, like `/bin/ls`. The following arguments are supplied
to the binary.

For example, running `ls` on all the nodes looks like this:
```
ansible all -a "ls"
```

For a simple example, let's take a look at installing htop.

To install it only on the central node:
```
ansible all -a "apt-get install htop" --become
```
The --become flag is to become the superuser for the install instead
of passing sudo priveleges over the ssh connection

This will work, however, Ansible will warn you that using the actual
"apt" package is probably a better idea.

The documentation for that is [here](https://docs.ansible.com/ansible/latest/modules/apt_module.html).
In order to pass in the module arguments, it looks a bit like this:
```
ansible all -m "apt" -a "update_cache=yes name=htop" --become
```


