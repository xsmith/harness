#!/usr/bin/env zsh

START_DIR="$PWD"
# Directory the script is in
DIR="$(dirname "$(readlink -f "$0")")"

# User configuration:
# Configuration file
CONFIG="config.yml"
INSTALL=""
OUTPUT="$DIR/output"
CFG="${DIR}/ansible.cfg"

RED='\033[0;31m'
NC='\033[0m' # No Color
CYAN='\033[0;36m'


function usage {
  echo "go-button [-h|--help] [-c|--config <file path>] [-i|--install <file path>]"
  echo " -h|--help:      Display this help message and exit."
  echo " -c|--config:    Configuration file containing variables for both the"
  echo "                 ansible and harness runtimes."
  echo "                 The default file is 'config.yml'."
  echo " -i|--install:   Ansible playbook containing install tasks to run."
  echo " --output:       Directory for ansible log files."
  echo "                 The default is $HARNESS/experiment/output."
}


if [[ "$(id -u)" != 0 ]]; then
    echo -e "\n${RED}Error:${NC} must be run as root\n"
    usage
    exit 1
fi


PARAMS=""
while (( $# )); do
  case "$1" in
    -c|--config)
      CONFIG=$2
      shift 2
      ;;
    -h|--help)
      usage
      exit 0
      ;;
    -i|--install)
      INSTALL=$2
      shift 2
      ;;
    --output)
      OUTPUT=$2
      shift 2
      ;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      usage
      exit 1
      ;;
    *) # preserve positional arguments
      # Oh traditional shells, how quaint.
      # This parameter handling is wrong, but I'm not going to fix it.
      # It's wrong because it's putting all the arguments into a single
      # string.  You can't get back the original argument list if any
      # arguments had spaces in them.
      PARAMS="$PARAMS $1"
      shift
      # But that nonsense aside, in reality we take no positional parameters,
      # so instead of silently ignoring them, let's exit with a help mesage.
      usage
      exit 1
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"


# clear out old output (still available on the hosts if you need it)
rm -rf "$OUTPUT"
mkdir -p "$OUTPUT"

# Make directory paths absolute, because these ansible scripts rely on
# the current directory.
OUTPUT="$(readlink -f $OUTPUT)"
CONFIG="$(readlink -f $CONFIG)"


echo "Starting experiment setup. This may take a while on the first run."
cd "$DIR"


# Set up the inventory file
./inventory-setup.zsh


# Set up the experiment
ANSIBLE_CONFIG="${CFG}" ansible-playbook $DIR/setup-experiment.yml > $OUTPUT/ansible-setup.log

if [ $? -eq 0 ]; then
  echo "Experiment setup done."
else
  echo "Setup failed. Exiting."
  exit 2
fi


if [[ -n $INSTALL ]]; then
  echo "Running experiment installation"
  ANSIBLE_CONFIG="${CFG}" ansible-playbook $INSTALL > $OUTPUT/ansible-install.log

  if [ $? -eq 0 ]; then
    echo "Experiment installation done."
  else
    echo "Installation failed. Exiting."
    exit 2
  fi
fi


echo "Starting testing campaign."

ANSIBLE_CONFIG="${CFG}" ansible-playbook $DIR/run-experiment.yml \
    --extra-vars "config_file=$CONFIG" \
    > $OUTPUT/ansible-run.log

# Chown the /local directory to match the current user to avoid needing sudo access when messing around after
./chown-all.sh > $OUTPUT/ansible-chown-all.log

if [ $? -eq 0 ]; then
  echo "Testing campaign finished!"
else
  echo "Testing campaign failed. See the ansible run log in the output directory."
  exit 2
fi
