#!/usr/bin/env bash

# Run this script from it's current directory
# A vast majority of this is cribbed from harness/experiment/go-button

RED='\033[0;31m'
NC='\033[0m' # No Color

if [[ "$(id -u)" != 0 ]]; then
    echo -e "\n${RED}Error:${NC} must be run as root\n"
    exit 1
fi

cd ../../experiment

CFG="./ansible.cfg"

./inventory-setup.zsh

echo "Starting experiment setup"

ANSIBLE_CONFIG="${CFG}" ansible-playbook ./setup-experiment.yml

if [ $? -eq 0 ]; then
  echo "Experiment setup done."
else
  echo "Setup failed. Exiting."
  exit 2
fi

echo "Starting experiment installation"
ANSIBLE_CONFIG="${CFG}" ansible-playbook ../configs/wasm-install-experiment.yml

if [ $? -eq 0 ]; then
  echo "Experiment installation done."
else
  echo "Installation failed. Exiting."
  exit 2
fi

