## Fuzzing profile setups for specific languages

In order to not have to reinstall everything every time you want to create a fuzzing
campaign, you can run the setup once, and save the resulting image. Then when reserving nodes
on Emulab, you can select your pre-built image instead of the general-purpose one.

# Setting up the image

- Run the installation how you normally would
- Save the image with a memorable name on Emulab
  - Make sure that the box asking if there are additional users/groups is ticked (elastic search, kibana,
    and logstash each have their own users)
- Copy the base harness profile
- Replace the image referenced in the profile with the new one
  - Pay particular care if it's the case that any of the nodes need a different image

# Repeatability

Make sure to put *all* of your installation steps into some kind of script. If things need
to be re-run or updated, having the script will make it easy.

# Updates

Certain fuzzing targets you might want to update to fix any reported/discovered bugs. It is
reccomended to write a little script that will update and build those targets before
a fuzzing campaign in order to avoid building a new image every time a target releases
an update.
