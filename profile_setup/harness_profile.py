"""Constructs an n-node cluster of machines for use as a harness for fuzzing
campaigns.

Instructions:
Instructions and information can be found at
the [Harness repository](https://www.google.com).
"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as emulab

pc = portal.Context()

pc.defineParameter("num_nodes", "Number of nodes",
                   portal.ParameterType.INTEGER, 2)

nodeLongDesc = "A specific hardware type to use for each node."

pc.defineParameter("osNodeType", "Hardware type of all nodes",
                   portal.ParameterType.NODETYPE, "d430",
                   longDescription=nodeLongDesc)

params = pc.bindParameters()

request = portal.context.makeRequestRSpec()

# Create a link with the type of LAN.
link = request.LAN("lan")

# Create nodes.
for i in xrange(params.num_nodes):
    node = request.RawPC("node%d" % i)
    if params.osNodeType:
        node.hardware_type = params.osNodeType
    node.disk_image = 'urn:publicid:IDN+emulab.net+image+randtest:xsmith-harness-ubuntu18'
    iface = node.addInterface("if%d" % (i+3))
    link.addInterface(iface)

portal.context.printRequestRSpec()