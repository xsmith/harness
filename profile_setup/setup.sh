#!/bin/zsh

# This script is meant to provide the dependencies
# for the Harness tools. For more specific software,
# such as the Elastic stack, the installation should
# be handled in the setup Ansible script.
# This helps keep the number of disk image changes
# down.


sudo apt update
# add the appility to add ppa repos
sudo apt install -y software-properties-common

# Prioritize the official Racket PPA
echo Installing Racket ...
sudo add-apt-repository --yes ppa:plt/racket
sudo apt update
sudo apt install -y racket

# Harness
# -------
# get the latest version of ansible
echo Installing Ansible ...
sudo apt-add-repository --yes ppa:ansible/ansible --update
sudo apt install -y ansible
sudo apt install -y aptitude

echo Installing Parallel ...
sudo apt install -y parallel
# parallel has a citation notice that can be removed
# with `parallel --citation`
echo Installing Python3.x ...
sudo apt install -y python3
sudo apt install -y python3-plumbum
sudo apt install -y python-is-python3

# Xsmith and csmith
# -----------------
echo Setting up Racket ...
sudo raco setup -j `nproc`
echo Installing cmake ...
sudo apt install -y cmake

# Elastic stack (logging)
# -----------------------
echo Installing Elastic Stack dependencies ...
sudo apt install -y openjdk-11-jdk
sudo apt install -y openjdk-11-jre

# Convenience tools
# -----------------
echo Installing convenience tools ...
sudo apt install -y htop
sudo apt install -y tmux
sudo apt install -y ncdu
sudo apt install -y ripgrep

# Cleanup
# -------
echo Cleaning up ...
sudo apt autoremove

