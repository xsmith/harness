#!/usr/bin/env bash


###### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! set the file!
#file=s2268581163.lua
#file=l1.lua
file=interesting.lua




if test "$#" -ne "0"; then
    echo 'Error!  Interesting script called with an argument!'
    echo 'Edit the file variable instead!'
    exit 13
fi


lua1=/luas/LuaJIT-2.0.5/src/luajit
lua2=/luas/lua-5.4.1/src/lua

#crashstring="attempt to index a nil value"
#crashstring="/src/lua"
#crashstring="attempt to call global 'right_arg_func' (a nil value)"
#crashstring="attempt to call global 'my_func' (a nil value)"


$lua1 $file >out1 2>&1
pret1=$?
$lua2 $file >out2 2>&1
pret2=$?


###### early exits for bad situations -- exit 1 is for uninteresting minimizations, exit 0 is a successful minimization.

# if either execution crashed, this was bad.
if test "$pret1" = "1"; then
    exit 1;
fi
if test "$pret2" = "1"; then
    exit 1;
fi


# printing a function directly -- we're getting an address
funcprint="function: 0x"
grep -q "$funcprint" out1
if test "$?" = "0"; then
    exit 1;
fi
grep -q "$funcprint" out2
if test "$?" = "0"; then
    exit 1;
fi

# printing a table directly -- we're getting an address
tableprint="table: 0x"
grep -q "$tableprint" out1
if test "$?" = "0"; then
    exit 1;
fi
grep -q "$tableprint" out2
if test "$?" = "0"; then
    exit 1;
fi


# no difference in outputs
diff -q out1 out2
if test "$?" = "0"; then
    exit 1;
fi





exit 0
