#!/usr/bin/env bash


###### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! set the file!
file=minimize.sml
cp reduction-candidate minimize.sml


if test "$#" -ne "0"; then
    echo 'Error!  Interesting script called with an argument!'
    echo 'Edit the file variable instead!'
    exit 13
fi


impl1=/smls/allrun/mlkit-git
impl2=/smls/allrun/mosmlc-v2.10.1


$impl1 $file >out1 2>&1
pret1=$?
$impl2 $file >out2 2>&1
pret2=$?

#grep -q "$crashstring" out1
#gret1=$?
#grep -q "$crashstring" out2
#gret2=$?


diff -q out1 out2
diffret=$?

#echo pret1 $pret1 pret2 $pret2 gret1 $gret1 gret2 $gret2 diffret $diffret
echo $pret1 $pret2 >/tmp/status
cp minimize.sml /tmp/reduce-file

rm minimize.sml
# 0 means interesting, 1 means uninteresting
if test "$pret1" != "0" -a "$pret2" = "0"; then
    exit 0;
else
    exit 1;
fi

