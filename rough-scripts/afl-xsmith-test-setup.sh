#!/usr/bin/env bash

# This one needs to be run as root.
# This sets up stuff so AFL will run happily...
echo performance | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
