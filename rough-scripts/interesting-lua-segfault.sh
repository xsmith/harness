#!/usr/bin/env bash


###### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! set the file!
file=interesting.lua
cp reduction-candidate interesting.lua


if test "$#" -ne "0"; then
    echo 'Error!  Interesting script called with an argument!'
    echo 'Edit the file variable instead!'
    exit 13
fi


lua1=/luas/ljx-LJX-vgit/src/luajit-ljx
lua2=/luas/lua-5.4.1/src/lua

#crashstring="attempt to index a nil value"
#crashstring="/src/lua"
#crashstring="attempt to call global 'right_arg_func' (a nil value)"
crashstring="attempt to call global 'my_func' (a nil value)"


$lua1 $file >out1 2>&1
pret1=$?
$lua2 $file >out2 2>&1
pret2=$?

grep -q "$crashstring" out1
gret1=$?
grep -q "$crashstring" out2
gret2=$?


diff -q out1 out2
diffret=$?

#echo pret1 $pret1 pret2 $pret2 gret1 $gret1 gret2 $gret2 diffret $diffret

rm interesting.lua

# 0 means interesting, 1 means uninteresting
#if test "$pret1" = "1" -a "$pret2" = "0" -a "$gret1" = "0" -a "$gret2" = "1" -a "$diffret" = "1"; then
if test "$pret1" = "139" -a "$pret2" = "0"; then
    exit 0;
else
    exit 1;
fi

