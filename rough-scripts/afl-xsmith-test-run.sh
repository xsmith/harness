#!/usr/bin/env bash

helpexit(){
    echo "usage: $0 id [clothoize|randomize] [xsmith-opt ...]"
    exit 1
}

if [[ "$#" -lt 2 ]]; then
    helpexit
fi

TESTID="$1"
shift

IGNORE_ARG="f"
if [[ "$1" == "clothoize" ]]; then
    IGNORE_ARG="f"
elif [[ "$1" == "randomize" ]]; then
    IGNORE_ARG="t"
else
    helpexit
fi
shift

cd /afl-test/test-dir
mkdir -p "$TESTID"
cd "$TESTID"

# We need some input files, let's just make some dumb seeds.
# I'm not sure what size of seed is most appropriate, but these make some deterministic, smallish, but not too-small files.
rm -rf inputs outputs
mkdir inputs outputs
for index in $(seq 30); do
    head -c 50 /dev/urandom >"inputs/input${index}"
done
#seq 0 100 >inputs/input1
#seq 50 100 >inputs/input2
#seq 0 2 100 >inputs/input3
#seq 100 -2 0 >inputs/input4

export AFL_FIX_SOCK="$PWD/aflfix.sock"
export AFL_POST_LIBRARY=/afl-test/aflfix/ext/post_shim.so

racket -l xsmith-examples/simple/lua -- --netstring-server "$AFL_FIX_SOCK" --netstring-ignore-input "$IGNORE_ARG" "$@" &
# wait to be sure that's definitely set up
sleep 4
afl-fuzz -i inputs -o outputs -t 3000 ../../lua/lua
kill %1
