#!/usr/bin/env bash

sudo apt install -y afl clang libreadline-dev
sudo mkdir /afl-test
sudo chown "$USER":users /afl-test
cd /afl-test
git clone https://github.com/bnagy/aflfix
pushd aflfix
git checkout 61ada5888196e7e6406767d83b61b2ef98241e4f
cd ext

file-edit(){
    cat "$1" | sed "$2" > tempfilename && mv -f tempfilename "$1"
}

# Add -fPIC to makefile so it will build...
file-edit Makefile 's/-o post_shim.so$/-o post_shim.so -fPIC/'
# out of expedience let's use strncpy instead of strlcpy
file-edit post_shim.so.c 's/strlcpy/strncpy/'
CC=clang make
popd

git clone https://github.com/lua/lua
pushd lua
git checkout v5.4.0
# The lua makefile hard-codes CC as gcc...
file-edit makefile 's/ gcc/ afl-gcc/'
make
popd

mkdir test-dir
pushd test-dir

