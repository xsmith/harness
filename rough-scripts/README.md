This is a directory of slapdash scripts written to help with running
fuzz campaigns.  They are not general, polished, good, or necessarily
useful to anyone but the author.  But maybe they will be slightly
helpful, and at any rate it will be nice to have them in version
control.
