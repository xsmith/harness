#!/usr/bin/env bash

apt update
apt install --yes smlsharp smlnj polyml libpolyml-dev mlton

#ml-burg ?


# smlnj binary is just `sml`
# polyml binary is `poly` or `polyc` -- they are both there, I'm not sure what the difference is.
# you can run `poly --script <file>` to run top-level declarations
# you can run `polyc -o <output-file> <file>` to compile an executable that has a main function

# you can run `mlton -output <output-file> <file>` to compile an executable -- no main function required


# instructions for how to make a heap image for smlnj: https://stackoverflow.com/questions/5053149/sml-nj-how-to-compile-standalone-executable
# smlnj doesn't compile to stand-alone executables, but there is a way to save/restore a heap.

# it seems some SMLs need a main function and others don't...
